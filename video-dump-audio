#!/bin/bash
#
# written by: woohoo
#
# dumps audio tracks from video files.
# depends on: zenity, mplayer, sed, awk, ts
#
# param 1: the folder path
# param 2: (optional) the output file extension
# param 3: (optional) for mp3, the output bitrate
#

declare -i NUMTHREADS=4
SECONDS=0

CRTPATH=$PWD
DIRTODO=$1
EXTOUT=*
KBPS=128

declare -i NUMPROCESSED=0

OLD_IFS=${IFS}
IFS='
'

if (($# >= 2)); then
  echo "extension passed: $2"
  EXTOUT=$2
fi

if (($# >= 3)); then
  echo "kbps passed: $3"
  KBPS=$3
fi

function prepare() {

  echo
  if [[ -d "${DIRTODO}" ]]; then
    echo "Dumping audio tracks from video files in directory: ${DIRTODO}" | ts
  elif [[ -f "${DIRTODO}" ]]; then
    echo "Dumping audio track from video file: ${DIRTODO}" | ts
  else
    echo "File or directory not found: ${DIRTODO}"
    exit 1
  fi

  # calculate how many threads we want to use:
  declare -i totalThreads=$(nproc --all)
  echo "Total threads available: ${totalThreads}."
  if ((totalThreads > 4)); then
    NUMTHREADS=$((totalThreads / 2))
  else
    NUMTHREADS=1
  fi
  echo "Will use: ${NUMTHREADS} threads."
}

# this function runs in threads - must not make use of
# any global variable
function processFile() {
  local file=$1
  echo "Processing file: ${file}..."

  local fn=$(readlink -f "${file}")

  local probeCtx=$(ffprobe "${fn}" 2>&1)

  local outext=

  if [[ "aa${EXTOUT}aa" == "aa*aa" ]]; then
    outext=$(echo "${probeCtx}" | egrep -i 'stream.+audio' | sed -E 's/^.*stream.*audio\W+(\w+).*/\1/i')
  else
    outext=${EXTOUT}
  fi

  #echo "outext=$outext."

  local s1='s/\.'
  local s2='$/\.'
  local s3='/gI'

  local inputext=${fn##*.}

  local sedexpr="${s1}${inputext}${s2}${outext}${s3}"
  local dest=$(echo "${fn}" | sed -E ${sedexpr})

  #echo "input : $fn."
  #echo "output: $dest."

  #mplayer way doesn't work:'
  #mplayer -dumpaudio -dumpfile "${dest}" "$fn" &> /dev/null

  if [[ "aa${EXTOUT}aa" == "aamp3aa" ]]; then
    ffmpeg -i "${fn}" -vn -ab ${KBPS}k -ar 44100 -y "${dest}" &>/dev/null
  else
    ffmpeg -i "${fn}" -vn -acodec copy "${dest}" &>/dev/null
  fi

  local exitstatus=$?
  if [ ${exitstatus} = 0 ]; then
    echo "+ Success, extracted audio file: ${dest}"
  else
    echo "! Failed to extract audio from: ${file}"
  fi
}

function processFiles() {

  # see if we have a directory or a file on hand:
  if [[ -d "${DIRTODO}" ]]; then
    cd "${DIRTODO}"
    echo -n "Processing directory: ${DIRTODO}..."

    local regexpr='.+\.\(avi\|f4v\|flv\|m4v\|mkv\|mov\|mp4\|mpeg\|ogv\|vob\|webm\|wmv\)$'

    local FILES=
    readarray -d $'\0' FILES < <(find "${DIRTODO}" -type f -iregex ${regexpr} -print0 | sort -zfV)

    declare -i numfiles=${#FILES[@]}
    echo "Found ${numfiles} files."

    declare -i crtbatch=0
    for file in "${FILES[@]}"; do
      ((NUMPROCESSED += 1))
      ((crtbatch += 1))

      if ((NUMTHREADS > 1)); then
        processFile "${file}" &

        if ((crtbatch >= NUMTHREADS || NUMPROCESSED >= numfiles)); then
          let crtbatch=0
          wait
        fi
      else
        processFile "${file}"
      fi

    done

  elif [[ -f "${DIRTODO}" ]]; then
    ((NUMPROCESSED += 1))
    processFile "${DIRTODO}"
  else
    echo "File or directory not found: ${DIRTODO}"
    exit 1
  fi
}

function finalize() {
  cd "${CRTPATH}"
  local elapsedtime="Running time: $((${SECONDS} / 3600))h $(((${SECONDS} / 60) % 60))m $((${SECONDS} % 60))s"
  echo

  IFS=$'\n'
  local msg="Finished dumping audio tracks from video files.${IFS}Processed ${NUMPROCESSED} files.${IFS}${elapsedtime}"
  IFS=${OLD_IFS}

  echo "${msg}" | ts
  echo
  notify-send -i info Information "${msg}"
  zenity --info --width=400 --text="${msg}" &>/dev/null
}

function main() {
  # read -p "Press any key to continue: " decision
  prepare

  processFiles

  finalize
}

############
### main ###
############

main

exit 0
