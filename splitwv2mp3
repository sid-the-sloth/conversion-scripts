#!/bin/bash
#
# splitwv2mp3
#
# this script depends on these packages: wavpack, cuetools, shntool, lame.
#
# split monolithic wv with cue and tag split files according to cue file, then
# convert them to mp3.
# copy this file and run it in the directory containing the monolithic wv and cue
#
# you need zenity package for notifications.
# please note that you can configure this script as a Thunar custom action
# and it will show up in right-click menu in any folder in Thunar.
#
# modify the lame options to your preference, for example, change: -b 320 to -b 128 or -b 192 or -b 256
# LAME_OPTS="-b 256 -h --cbr"
# LAME_OPTS="-b 320 -h -V 0 --vbr-new"
#
# change lame encoding options here:
LAME_OPTS="-b 320 -h --cbr"

old_IFS=${IFS}
IFS='
'

crtpath=$PWD
cd "$1"

# phase 1: split big wav file
splitdir="Split Tracks"
rm -f -s "$splitdir"
mkdir "$splitdir"

# copy the cue file
cuefiles=$(find . -type f -iregex '^.+\.cue$' | sort -fV)
thecue=""
for file in ${cuefiles}
do
  fn=$(readlink -f "$file")
  if [ -f "$fn" ]; then
    thecue=$fn
    cp "$fn" "$splitdir/copy.cue"
    break
  fi
done

shnsplit -o wv -f "$thecue" -t "%n - %t" -d "$splitdir" *.wv

cd "$splitdir"
# sometimes we end up with a garbage file called 00 - pregap.wv:
rm -f "00 - pregap.wv"

#phase 2: convert wv tracks to wav
files=$(find . -type f -iregex '^.+\.wv$' | sort -fV)
for file in ${files}
do
  fn=$(readlink -f "$file")
  if [ -f "$fn" ]; then
    dest=$(echo "$fn"|sed -E 's/\.wv$/\.wav/gI')
    rm -f "$dest"
    wvunpack -y "$fn" -o "$dest"
  fi
done

rm -f *.wv

#phase 3: convert wav tracks to mp3
files=$(find . -type f -iregex '^.+\.wav$' | sort -fV)
declare -i nn=0

for file in ${files}
do
  fn=$(readlink -f "$file")
  if [ -f "$fn" ]; then
    dest=$(echo "$fn"|sed -E 's/\.wav$/\.mp3/gI')
    rm -f "$dest"
    lame ${LAME_OPTS} "$fn" "$dest"
    ((nn+=1))
  fi
done

# write tags - too bad this only updates id3v1 tags, what to do for id3v2 tags?
# now i have my own version of cuetag which updates id3v2
cuetag copy.cue *.mp3

# cleanup
rm -f copy.cue
rm -f *.wav

cd "${crtpath}"
zenity --info --width=400 --text="Finished splitting and converting wv to mp3.${IFS}Created ${nn} mp3 files."
IFS=${old_IFS}
