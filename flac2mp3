#!/bin/bash
#
# Yet(!) Another FLAC to MP3 script
#
# Distributed under the terms of the GNU General Public License v3
#
# depends on: lame flac metaflac sed ts nproc zenity notify-send
# please note that you can configure this script as a Thunar custom action
# and it will show up in right-click menu in any folder in Thunar.
#
# modify the lame options to your preference example change -b 320 to -b 128 or -b 192 or -b 256
# LAME_OPTS="--vbr-new -V 0 -b 256"
# LAME_OPTS="-V 0 --vbr-new"

declare -i NUMTHREADS=4
SECONDS=0
INPUTEXT=flac
OUTPUTEXT=mp3

LAME_OPTS="-b 320 -q 0 --cbr"

CRTPATH=$PWD
DIRTODO=$1
declare -i NUMPROCESSED=0

OLD_IFS=${IFS}
IFS='
'

if (($# >= 2)); then
  echo "Starting number of seconds passed: $2"
  SECONDS=$2
fi

function prepare() {
  cd "${DIRTODO}"
  echo
  echo "Converting ${INPUTEXT} to ${OUTPUTEXT} in directory: ${DIRTODO}" | ts

  # calculate how many threads we want to use:
  declare -i totalThreads=$(nproc --all)
  echo "Total threads available: ${totalThreads}."
  if ((totalThreads > 4)); then
    NUMTHREADS=$((totalThreads / 2))
  else
    NUMTHREADS=1
  fi
  echo "Will use: ${NUMTHREADS} threads."
}

# this function runs in threads - must not make use of
# any global variable
function processFile() {
  local file=$1
  echo "Processing file: ${file}..."

  local id3v2=$(which id3v2)
  local fn=$(readlink -f "${file}")
  local vars=($(metaflac --export-tags-to=- "${fn}"))
  # --no-utf8-convert

  for N_vars in ${!vars[@]}; do
    export "$(echo "${vars[${N_vars}]%=*}" | tr [:upper:] [:lower:])=${vars[${N_vars}]#*=}"
  done

  local s1='s/\.'
  local s2='$/\.'
  local s3='/gI'
  local sedexpr="${s1}${INPUTEXT}${s2}${OUTPUTEXT}${s3}"
  local dest=$(echo "${fn}" | sed -E ${sedexpr})

  flac -dcs "${fn}" |\
    lame --ignore-tag-errors --add-id3v2 ${LAME_OPTS} \
      "${artist:+--ta}" "${artist}" \
      "${tracknumber:+--tn}" "${tracknumber}" \
      "${title:+--tt}" "${title}" \
      "${album:+--tl}" "${album}" \
      "${date:+--ty}" "${date}" \
      "${genre:+--tg}" "${genre}" \
      "${comment:+--tc}" "${comment}" \
      - "${dest}"

  [[ -x "${id3v2}" ]] && "${id3v2}" \
    "${artist:+--artist}" "${artist}" \
    "${tracknumber:+--track}" "${tracknumber}" \
    "${title:+--song}" "${title}" \
    "${album:+--album}" "${album}" \
    "${date:+--year}" "${date}" \
    "${genre:+--genre}" "${genre}" \
    "${comment:+--comment}" "${comment}" \
    "${dest}"
}

function processFiles() {

  local r1='^.+\.'
  local r2='$'
  local regexpr="${r1}${INPUTEXT}${r2}"
  local FILES=
  readarray -d $'\0' FILES < <(find "${DIRTODO}" -type f -iregex "${regexpr}" -print0 | sort -zfV)

  declare -i numfiles=${#FILES[@]}
  echo "Found ${numfiles} files."

  declare -i crtbatch=0
  for file in "${FILES[@]}"; do
    ((NUMPROCESSED += 1))
    ((crtbatch += 1))

    if ((NUMTHREADS > 1)); then
      processFile "${file}" &

      if ((crtbatch >= NUMTHREADS || NUMPROCESSED >= numfiles)); then
        let crtbatch=0
        wait
      fi
    else
      processFile "${file}"
    fi
  done
}

function finalize() {
  cd "${CRTPATH}"
  local elapsedtime="Running time: $((${SECONDS} / 3600))h $(((${SECONDS} / 60) % 60))m $((${SECONDS} % 60))s"
  echo

  IFS=$'\n'
  local msg="Finished converting ${INPUTEXT} to ${OUTPUTEXT}.${IFS}Processed ${NUMPROCESSED} files.${IFS}${elapsedtime}"
  IFS=${OLD_IFS}

  echo "${msg}" | ts
  echo
  notify-send -i info Information "${msg}"
  zenity --info --width=400 --text="${msg}" &>/dev/null
}

function main() {
  # read -p "Press any key to continue: " decision
  prepare

  processFiles

  finalize
}

############
### main ###
############

main

exit 0
