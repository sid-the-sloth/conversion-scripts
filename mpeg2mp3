#!/bin/bash
#
# Author: woohoo
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY. YOU USE AT YOUR OWN RISK. THE AUTHOR
# WILL NOT BE LIABLE FOR DATA LOSS, DAMAGES, LOSS OF PROFITS OR ANY
# OTHER KIND OF LOSS WHILE USING OR MISUSING THIS SOFTWARE.
# See the GNU General Public License for more details.
#
# Now with multithreading for faster processing
#
# you need zenity package for notifications.
# depends on ffmpeg.
# please note that you can configure this script as a Thunar custom action
# and it will show up in right-click menu in any folder in Thunar.
#
# modify the kbps option to your preference example change "256k" to "320k"
# LAME_OPTS=256k


declare -i NUMTHREADS=4
SECONDS=0
INPUTEXT=mpeg
OUTPUTEXT=mp3

LAME_OPTS=128k

CRTPATH=$PWD
DIRTODO=$1
declare -i NUMPROCESSED=0

OLD_IFS=${IFS}
IFS='
'

if (( $# >= 2 )); then
  echo "kbps passed: $2"
  LAME_OPTS=$2
fi

function prepare(){
  cd "${DIRTODO}"
  echo
  echo "Converting ${INPUTEXT} to ${OUTPUTEXT} in directory: ${DIRTODO}"

  # calculate how many threads we want to use:
  declare -i totalThreads=$(nproc --all)
  echo "Total threads available: ${totalThreads}."
  if ((totalThreads > 4)); then
    NUMTHREADS=$((totalThreads/2))
  else
    NUMTHREADS=1
  fi
  echo "Will use: ${NUMTHREADS} threads."
}

# this function runs in threads - must not make use of
# any global variable
function processFile(){
  local file=$1
  echo "Processing file: ${file}..."

  local fn=$(readlink -f "${file}")

  local s1='s/\.'
  local s2='$/\.'
  local s3='/gI'
  local sedexpr="${s1}${INPUTEXT}${s2}${OUTPUTEXT}${s3}"
  local dest=$(echo "${fn}"|sed -E ${sedexpr})

  ffmpeg -i "${fn}" -vn -ar 44100 -ac 2 -b:a ${LAME_OPTS} "${dest}"
}

function processFiles(){
  local r1='^.+\.'
  local r2='$'
  local regexpr="${r1}${INPUTEXT}${r2}"
  local FILES=
  readarray -d $'\0' FILES < <(find "${DIRTODO}" -type f -iregex ${regexpr} -print0 | sort -zfV)

  declare -i numfiles=${#FILES[@]}
  echo "Found ${numfiles} files."

  declare -i crtbatch=0
  for file in "${FILES[@]}"; do
    ((NUMPROCESSED+=1))
    ((crtbatch+=1))

    if ((NUMTHREADS>1)); then
      processFile "${file}" &

      if (( crtbatch >= NUMTHREADS || NUMPROCESSED >= numfiles )); then
        let crtbatch=0
        wait
      fi
    else
      processFile "${file}"
    fi

  done
}

function finalize(){
  cd "${CRTPATH}"
  local elapsedtime="Running time: $((${SECONDS} / 3600))h $(((${SECONDS} / 60) % 60))m $((${SECONDS} % 60))s"
  echo

  IFS=$'\n'
  local msg="Finished converting ${INPUTEXT} to ${OUTPUTEXT}.${IFS}Processed ${NUMPROCESSED} files.${IFS}${elapsedtime}"
  IFS=${OLD_IFS}

  echo "${msg}" | ts
  echo
  notify-send -i info Information "${msg}"
  zenity --info --width=400 --text="${msg}" &> /dev/null
}


function main(){
  # read -p "Press any key to continue: " decision
  prepare

  processFiles

  finalize
}

############
### main ###
############

main

exit 0
