/*
 * This nodejs script will convert a tokens.xml file
 * exported from old FreeOTP android app by RedHat
 * into a json file needed to be imported in FreeOTP+ or FreeOTPPlus
 * a free and open-source app for android.
 * 
 * The android app can be installed from:
 * https://play.google.com/store/apps/details?id=org.liberty.android.freeotpplus&hl=en_US&gl=US
 * 
 * Its code repo is here:
 * https://github.com/helloworld1/FreeOTPPlus
 * 
 * Make sure you run this before:
 * npm install system fs path
 *
 * usage:
 * run with the full path to the tokens.xml file:
 * > node freeotp-xml-to-json.js /path/to/my/tokens.xml
 * 
 * It will create a file in the same directory, called: freeotpplus-backup-<guid>.json
 */

const system = require('system');
const fs = require('fs');
const path = require('path');

/*
 * Generates a GUID string.
 * @returns {String} The generated GUID.
 * @example af8a8416-6e18-a307-bd9c-f2c947bbb3aa
 * @author Slavik Meltser (slavik@meltser.info).
 * @link http://slavik.meltser.info/?p=142
 */
function guid() {
    function _p8(s) {
        var p = (Math.random().toString(16) + "000000000").substr(2,8);
        return s ? "-" + p.substr(0,4) + "-" + p.substr(4,4) : p;
    }
    return _p8() + _p8(true) + _p8(true) + _p8();
}

var tokensFile = null;

if (typeof process.argv !== 'undefined' && process.argv.length > 2) {
    // 0 index is the process path '/opt/nodejs/bin/node'
    // 1 index is the script file name itself '/path/to/this/file/freeotp-xml-to-json.js'
    var tokensFile = process.argv[2].toString();
    console.log('File passed: ' + tokensFile + '.');

    try {
        fs.accessSync(tokensFile, fs.constants.F_OK);
    } catch (err) {
        console.error(err);
        process.exit(1);
    }

    console.log('Processing...');
} else {
    console.log('No tokens file passed in. Exiting.');
    process.exit(1);
}

function processTokensXml(data) {
    var reXml = /\s*<\?xml[^<>]+>\s*/gim;
    var reMap = /\s*<\/?map>\s*/gim;
    var reTokenOrder = /^\s*<string\s+name="tokenOrder">.*$/gim;
    var reLineBegin = /^\s*<[^<>]+>/gim;
    var reLineEnd = /\s*<[^<>]+>$/gim;
    var reQuot = /\&quot\;/gim;
    
    var tokensArray = data.replace(reXml, '');
    tokensArray = tokensArray.replace(reMap, '');
    
    var tokenOrderLine = reTokenOrder.exec(tokensArray)[0];
    tokenOrderLine = tokenOrderLine.replace(reLineBegin, '');
    tokenOrderLine = tokenOrderLine.replace(reLineEnd, '');
    tokenOrderLine = tokenOrderLine.replace(reQuot, '"');

    tokensArray = tokensArray.replace(reTokenOrder, '');
    tokensArray = tokensArray.replace(reLineBegin, '');
    tokensArray = tokensArray.replace(reLineEnd, '');
    tokensArray = tokensArray.replace(reQuot, '"');
    tokensArray = tokensArray.replace(/$/gim, ',');
    tokensArray = '[' + tokensArray + ']'
    tokensArray = tokensArray.replace(/\}\s*,\s*\]/gim, '}]');
    tokensArray = tokensArray.replace(/\n/gim, '');

    var result = '{"tokenOrder":' + tokenOrderLine + ',' + '\n"tokens":' + tokensArray + '}\n';
    return result;
}

function writeToFile(filePath, data) {
    fs.writeFile(filePath, data, function (err) {
        if (err) {
            console.log('Error while writing to file: ' + filePath + '.');
            throw err;
        } else {
            console.log('File written successfully: ' + filePath + '.');
        }
    });
}

fs.readFile(tokensFile, 'utf8', function(err, data) {

    if (err) return console.error(err);

    var fileWithExt = path.basename(tokensFile);
    var fileDir = path.dirname(tokensFile);
    var fileExt = path.extname(tokensFile);
    
    if (fileExt.toLowerCase() != '.xml') {
        console.log('Error: file must be a proper XML file. Exiting.');
        process.exit(1);
    }
    
    var fileNoExt = path.basename(tokensFile, fileExt);

    var result = processTokensXml(data);
    
    var newfileName = 'freeotpplus-backup-' + guid() + '.json';
    var newFilePath = path.join(fileDir, newfileName);

    writeToFile(newFilePath, result);

    console.log('*** Done. ***');
    console.log('');

});
